FROM openjdk:8-jdk-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY target/notification-service-0.0.1-SNAPSHOT.jar /usr/src/app
# Bundle app source
EXPOSE 8081
#CMD ["npm","start"]
ENTRYPOINT ["java","-jar","notification-service-0.0.1-SNAPSHOT.jar"]
