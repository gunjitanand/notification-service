package com.adidas.notificationservice.model;

public class SubscriptionModel {
	
	private String email;
	private String firstName;
	private String gender;
	private String consentFlag;
	private Long newsLetterId;
	
	
	public Long getNewsLetterId() {
		return newsLetterId;
	}
	public void setNewsLetterId(Long newsLetterId) {
		this.newsLetterId = newsLetterId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getConsentFlag() {
		return consentFlag;
	}
	public void setConsentFlag(String consentFlag) {
		this.consentFlag = consentFlag;
	}

}
