package com.adidas.notificationservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adidas.notificationservice.model.SubscriptionModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(value="subscriptionplatform", description="handles notification operations")
@RestController
public class NotificationServiceController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceController.class);

	
	@ApiOperation(value = "sends the notification to the customer", response = Iterable.class)
	@PostMapping("/notification/create")
	public ResponseEntity<String> createUser(@RequestBody SubscriptionModel model) {
		
		LOGGER.debug("message received in notification service " + model.getEmail());
		//TODO:: call to a service which sends out the notification
		return ResponseEntity.ok("notification sent");
	}

	
	

}
